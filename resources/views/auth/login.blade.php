@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col m8 offset-m2 m-t-3">
            <h3 class="h3 center-align">Login</h3>
            <div class="card cyan accent-2 white-text">
                <div class="card-content white-text">
                    <form id="login-form" class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="input-field {{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label white-text">E-Mail Address</label>


                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                            @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>

                        <div class="input-field {{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 white-text control-label">Password</label>

                            <input id="password" type="password" class="form-control" name="password" required>

                            @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
                        <p>
                            <input type="checkbox" id="remember" name="remember" {{ old('remember') ? 'checked' : '' }} /> 
                                   <label for="remember" class="white-text">Запомнить меня</label>
                        </p>
                    </form>
                </div>
                <div class="card-action">
                    <div class="input-field">
                        <div class="col-md-8 col-md-offset-4">
                            <button type="submit" form="login-form" class="btn waves-effect waves-light green">
                                Вход
                            </button>

                            <a class="btn waves-effect waves-light yellow black-text" href="{{ route('password.request') }}">
                                Забыли пароль?
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
