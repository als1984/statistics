<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
        <!-- Styles -->
        <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
    </head>
    <body>
        <header>
        <ul id="dropdown1" class="dropdown-content">
            <li>
                <a href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();">
                    Logout
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>
        </ul>
        <nav>
            <div class="nav-wrapper green">
                <a href="{{ url('/') }}" class="brand-logo">{{ config('app.name', 'Statistics') }}</a>
                <ul class="right hide-on-med-and-down">
                    <!-- Authentication Links -->
                    @guest
                    <li><a href="{{ route('login') }}">Login</a></li>
                    <li><a href="{{ route('register') }}">Register</a></li>
                    @else
                    <li class="dropdown">
                    <li><a class="dropdown-button" href="#!" data-activates="dropdown1">{{ Auth::user()->name }}<i class="material-icons right">arrow_drop_down</i></a></li>
                    </li>
                    @endguest
                </ul>
            </div>
        </nav>
        </header>
        <main>
        @yield('content')
        </main>
        <footer class="page-footer">
            <div class="footer-copyright">
                <div class="container">
                    <a href="https://grizzly.by">
                        <img class="logo-foot" title="GRIZZLY DIGITAL COMPANY" alt="Логотип GRIZZLY DIGITAL COMPANY" src="https://grizzly.by/assets/template/images/logo.png" float="left" width="30">
                        <p class="foot-name">GRIZZLY.BY</p>
                        <p class="copyrights">2018 All rights reserved</p>
                    </a>
                </div>
            </div>
        </footer>
        
        <!-- Scripts -->
        <script  src="http://code.jquery.com/jquery-3.2.1.min.js"  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="  crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
        <script src="{{ asset('js/site.js') }}"></script>
    </body>
</html>
