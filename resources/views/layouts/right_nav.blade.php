<ul id="slide-out" class="side-nav">
    <li>
        <div class="user-view">
            <img src="https://grizzly.by/assets/template/images/logo_black.png">
            <h3 class="red-text">Меню</h3>
        </div>
    </li>
    @foreach ($roles as $role)
        @if($role->name == 'admin')
            <li><a href="{{ route('users') }}"><span>Пользователи</span></a></li>
            <li><a href="{{ route('all_projects') }}">Все проекты</a></li>
            <li><a href="{{ route('event_type') }}"><span >Типы Событий</span></a></li> 
        @endif
        @if($role->name == 'manager')
            <li><a href="{{ route('projects.index') }}"><span >Мои Проекты</span></a></li>
                       
        @endif
        @if($role->name == 'user')
            <li><a href="{{ route('events') }}"><span >События</span></a></li>
            <li><a href="{{ route('my_page') }}"><span >Страницы</span></a></li>
        @endif
    @endforeach
</ul>

