@extends('layouts.app')

@include('layouts.right_nav')
@section('content')
<div class="container">
    <div class="m-t-1">
        <a class="btn-floating btn-large waves-effect waves-light blue right_nav" data-activates="slide-out"><i class="material-icons">menu</i></a>
        <a href="#add-modal" class="btn-floating btn-large waves-effect waves-light green modal-trigger right" ><i class="material-icons">add</i></a>
    </div>
    <div class="row">
        <div class="col m8 offset-m2">
            <h4 class="center-align">Страницы</h4>
            <div class="m-t-3">
                <ul class="collapsible" data-collapsible="accordion">
                    @foreach($pages as $page)
                    <li>
                        <div class="collapsible-header"><i class="material-icons">event</i>{{$page->page_name}}</div>
                        <div class="collapsible-body">
                            <form class="edit-page" action="{{ route('edit_event')}}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{$page->id}}">
                                <div class="input-field">
                                    <input type="text" id="name{{$page->id}}" name="name" required value="{{$page->page_name}}">
                                    <label for="name{{$page->id}}" >Имя</label>
                                </div>
                                <div class="input-field">
                                    <input type="text" id="url{{$page->id}}" name="url" required value="{{$page->page_url}}">
                                    <label for="element{{$page->id}}" >Ссылка</label>
                                </div>                                
                                <div class="input-field">
                                    <select id="event_type{{$page->id}}" name="project_id" required>  
                                        <option value="">Выберите проект</option>        
                                        @foreach($projects as $project_item)
                                            @if($project)
                                                <option value="{{$project_item->id}}" @if($project->id==$project_item->id) selected="selected" @endif >{{$project_item->project_name}}</option>
                                            @else
                                                <option value="{{$project_item->id}}" @if($page->project_id==$project_item->id) selected="selected" @endif >{{$project_item->project_name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                    <label for="event_type{{$page->id}}">Проект</label>
                                </div> 
                                <button type="submit" class="btn waves-effect black-text waves-light yellow">Изменить</button>
                                <a href="{{ route('delete_event')}}" id="del_{{$page->id}}" class="right-align event_delete tooltipped right" data-position="bottom" data-delay="50" data-id="{{$page->id}}" data-tooltip="Удалить"><i class="delete_Event_type red-text material-icons">clear</i></a>
                            </form>
                        </div>
                    </li>   
                    @endforeach
                </ul>                
            </div>
        </div>        
    </div>
    @if($pagelinks)
    <div class="row">
        <div class="col m10">            
            {{ $pages->links() }}
        </div>
    </div>
    @endif
</div>
<div id="add-modal" class="modal">
    <div class="modal-content">
        <form id="add_page" action="{{ route('add_page')}}" method="POST">
            {{ csrf_field() }}
            <div class="input-field">
                <input type="text" id="name" name="name"  required >
                <label for="name" >Имя</label>
            </div>
            <div class="input-field">
                <input type="text" id="url" name="url" required value="">
                <label for="url" >Ссылка</label>
            </div>
            <div class="input-field">
                <select id="project" name="project_id" required>  
                    <option value="">Выберите проект</option>        
                    @foreach($projects as $project_item)
                        @if($project)
                            <option value="{{$project_item->id}}" @if($project->id==$project_item->id) selected="selected" @endif >{{$project_item->project_name}}</option>
                        @else
                            <option value="{{$project_item->id}}" @if($page->project_id==$project_item->id) selected="selected" @endif >{{$project_item->project_name}}</option>
                        @endif
                    @endforeach
                </select>
                <label for="project">Проект</label>
            </div> 
           

        </form>
    </div>
    <div class="modal-footer">
        <button type="submit" form="add_page" class="btn waves-effect waves-light green">Добавить</button>
    </div>
</div>
@endsection