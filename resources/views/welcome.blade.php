<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Statistics</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
            <div class="top-right links">
                @auth
                <a href="{{ url('/home') }}">Home</a>
                @else
                <a href="{{ route('login') }}">Login</a>
<!--                <a href="{{ route('register') }}">Register</a>-->
                @endauth
            </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Statistics
                </div>
                <div class="links">
                    <p>Сбор статистики пользователей</p>
                </div>
            </div>            
        </div>
        <footer>
            <div class="text-center">
                <a href="https://grizzly.by">
                    <img class="logo-foot" title="GRIZZLY DIGITAL COMPANY" alt="Логотип GRIZZLY DIGITAL COMPANY" src="https://grizzly.by/assets/template/images/logo.png" float="left" width="30">
                    <p class="foot-name">GRIZZLY.BY</p>
                    <p class="copyrights">2018 All rights reserved</p>
                </a>
            </div>
            </footer>
    </body>
</html>
