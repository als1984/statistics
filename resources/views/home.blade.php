@extends('layouts.app')

@include('layouts.right_nav')
@section('content')
<div class="container">
    <div class="m-t-1">
        <a class="btn-floating btn-large waves-effect waves-light blue right_nav" data-activates="slide-out"><i class="material-icons">menu</i></a>
    </div>
    <div class="row">
        <div class="col m8 offset-m2">
            <div class="m-t-3">
                <h2 class="center-align">Добро пожаловать {{$name}}</h2>
                <p class="center-align">Вы находитесь в панеле управления статистикой.</p>
            </div>
        </div>
    </div>
</div>
@endsection
