@extends('layouts.app')

@include('layouts.right_nav')
@section('content')
<div class="container">
    <div class="m-t-1">
        <a class="btn-floating btn-large waves-effect waves-light blue right_nav" data-activates="slide-out"><i class="material-icons">menu</i></a>
        <a href="#add-modal" class="btn-floating btn-large waves-effect waves-light green modal-trigger right" ><i class="material-icons">add</i></a>
    </div>
    <div class="row">
        <div class="col m8 offset-m2">
            <h4 class="center-align">Cобытия</h4>
            <div class="m-t-3">
                <ul class="collapsible" data-collapsible="accordion">
                    @forelse($events as $eventarr)
                    @foreach($eventarr as $event)
                    <li>
                        <div class="collapsible-header"><i class="material-icons">event</i>{{$event->event_name}}</div>
                        <div class="collapsible-body">
                            <form class="edit-event" action="{{ route('edit_event')}}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{$event->id}}">
                                <div class="input-field">
                                    <input type="text" id="name{{$event->id}}" name="name" required value="{{$event->event_name}}">
                                    <label for="name{{$event->id}}" >Имя</label>
                                </div>
                                <div class="input-field">
                                    <input type="text" id="element{{$event->id}}" name="element" required value="{{$event->event_element}}">
                                    <label for="element{{$event->id}}" >Селектор</label>
                                </div>
                                <div class="input-field">
                                    <input type="text" id="url_element{{$event->id}}" name="url_element" value="{{$event->event_url_element}}">
                                    <label for="url_element{{$event->id}}" >Селектор url (по-умолчанию url страницы)</label>
                                </div>
                                <div class="input-field">
                                    <input type="text" id="name_element{{$event->id}}" name="name_element" value="{{$event->event_name_element}}">
                                    <label for="name_element{{$event->id}}" >Селектор названия страницы (по-умолчанию title)</label>
                                </div>
                                <div class="input-field">
                                    <select id="project{{$event->id}}" name="project_id" required>  
                                        <option value="">Выберите проект</option>        
                                        @foreach($projects as $project)
                                        <option value="{{$project->id}}" @if($event->projects_id==$project->id) selected="selected" @endif >{{$project->project_name}}</option>
                                        @endforeach
                                    </select>
                                    <label for="project{{$event->id}}">Проект</label>
                                </div> 
                                <div class="input-field">
                                    <select id="event_type{{$event->id}}" name="event_type_id" required>  
                                        <option value="">Выберите тип события</option>        
                                        @foreach($events_types as $events_type)
                                        <option value="{{$events_type->id}}" @if($event->events_types_id==$events_type->id) selected="selected" @endif >{{$events_type->event_type_name}}</option>
                                        @endforeach
                                    </select>
                                    <label for="event_type{{$event->id}}">Тип события</label>
                                </div> 
                                <button type="submit" class="btn waves-effect black-text waves-light yellow">Изменить</button>
                                <a href="{{ route('delete_event')}}" id="del_{{$event->id}}" class="right-align event_delete tooltipped right" data-position="bottom" data-delay="50" data-id="{{$event->id}}" data-tooltip="Удалить"><i class="delete_Event_type red-text material-icons">clear</i></a>
                            </form>
                        </div>
                    </li>   
                    @endforeach
                    @empty
                    <li></li>
                    @endforelse
                </ul>                
            </div>
        </div>
    </div>
</div>
<div id="add-modal" class="modal">
    <div class="modal-content">
        <form id="add_event" action="{{ route('add_event')}}" method="POST">
            {{ csrf_field() }}
            <div class="input-field">
                <input type="text" id="name" name="name"  required >
                <label for="name" >Имя</label>
            </div>
            <div class="input-field">
                <input type="text" id="element" name="element" required value="">
                <label for="element" >Селектор</label>
            </div>
            <div class="input-field">
                <input type="text" id="url_element" name="url_element" value="">
                <label for="url_element" >Селектор url (по-умолчанию url страницы)</label>
            </div>
            <div class="input-field">
                <input type="text" id="name_element" name="name_element" value="">
                <label for="name_element" >Селектор названия страницы (по-умолчанию title)</label>
            </div>
            <div class="input-field">
                <select id="project" name="project_id" required>  
                    <option value="">Выберите проект</option>        
                    @foreach($projects as $project)
                    <option value="{{$project->id}}" >{{$project->project_name}}</option>
                    @endforeach
                </select>
                <label for="project">Проект</label>
            </div> 
            <div class="input-field">
                <select id="event_type" name="event_type_id" required>  
                    <option value="">Выберите тип события</option>        
                    @foreach($events_types as $events_type)
                    <option value="{{$events_type->id}}" >{{$events_type->event_type_name}}</option>
                    @endforeach
                </select>
                <label for="event_type">Тип события</label>
            </div> 

        </form>
    </div>
    <div class="modal-footer">
        <button type="submit" form="add_event" class="btn waves-effect waves-light green">Добавить</button>
    </div>
</div>
@endsection