@extends('layouts.app')

@include('layouts.right_nav')
@section('content')
<div class="container">
    <div class="m-t-1">
        <a class="btn-floating btn-large waves-effect waves-light blue right_nav" data-activates="slide-out"><i class="material-icons">menu</i></a>
    </div>
    <div class="row">
        <div class="col m8 offset-m2">

            <div>       
                <ul class="collection with-header">
                    <li class="collection-header"><h4>{{$project->project_name}}</h4></li>
                    <li class="collection-item">
                        <span class="blue-text text-darken-4">Ключ: </span>
                        {{$project->project_key}}
                    </li>
                    <li class="collection-item">
                        <span class="blue-text text-darken-4">Пользователи проекта: </span>
                        <a href="{{ route('project_users',['id'=>$project->id])}}" class="secondary-content"><i class="material-icons">send</i></a>
                    </li>
                    <li class="collection-item">
                        <span class="blue-text text-darken-4">События проекта: </span>
                        <a href="{{ route('project_events',['id'=>$project->id])}}" class="secondary-content"><i class="material-icons">send</i></a>
                    </li>
                    <li class="collection-item">
                        <span class="blue-text text-darken-4">Страницы проекта: </span>
                        <a href="{{ route('get_project_pages',['id'=>$project->id])}}" class="secondary-content"><i class="material-icons">send</i></a>
                    </li>
                </ul>
                <a class="btn waves-effect black-text waves-light yellow" href="{{ route('projects.edit',['project'=>$project->id])}}">Изменить</a>
                <form action="{{ route('projects.destroy',['project'=>$project->id])}}" method="POST">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <button type="submit" class="btn-flat right-align tooltipped right" data-position="bottom" data-delay="50" data-id="{{$project->id}}" data-tooltip="Удалить"><i class="red-text material-icons">clear</i></button>
                </form>                
            </div>
            <div class="m-t-1">
                @if ($errors->any())
                <div class="red">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection