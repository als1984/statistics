@extends('layouts.app')

@include('layouts.right_nav')
@section('content')
<div class="container">
    <div class="m-t-1">
        <a class="btn-floating btn-large waves-effect waves-light blue right_nav" data-activates="slide-out"><i class="material-icons">menu</i></a>
        <a href="{{ route('projects.create')}}" class="btn-floating btn-large waves-effect waves-light green right" ><i class="material-icons">add</i></a>
    </div>
    <div class="row">
        <div class="col m8 offset-m2">

            <div class="m-t-3">
                <ul class="collection with-header">
                    <li class="collection-header"><h4>Проекты</h4></li>
                    @foreach($projects as $project)
                    <li class="collection-item">
                        <div>{{$project->project_name}}<a href="{{ route('projects.show',['project'=>$project->id])}}" class="secondary-content"><i class="material-icons">send</i></a></div>
                    </li>   
                    @endforeach
                </ul>                
            </div>
        </div>
        <div class="col m10">
            {{ $projects->links() }}
        </div>
    </div>
</div>
@endsection