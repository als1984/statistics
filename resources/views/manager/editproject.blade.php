@extends('layouts.app')

@include('layouts.right_nav')
@section('content')
<div class="container">
    <div class="m-t-1">
        <a class="btn-floating btn-large waves-effect waves-light blue right_nav" data-activates="slide-out"><i class="material-icons">menu</i></a>
    </div>
    <div class="row">
        <div class="col m8 offset-m2">

            <div class="m-t-3">
                <form id="edit_progect" action="{{ route('projects.update',['project'=>$project->id])}}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <div class="input-field">
                        <input type="text" id="project_name" name="project_name" value="{{ $project->project_name}}"  required >
                        <label for="project_name" >Название проекта</label>
                    </div>                    
                    <button type="submit" form="edit_progect" class="btn waves-effect waves-light green">Изменить</button>
                </form>               
            </div>
            <div class="m-t-1">
                @if ($errors->any())
                <div class="red">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection