@extends('layouts.app')

@include('layouts.right_nav')
@section('content')
<div class="container">
    <div class="m-t-1">
        <a class="btn-floating btn-large waves-effect waves-light blue right_nav" data-activates="slide-out"><i class="material-icons">menu</i></a>
        <a href="#add-modal" class="btn-floating btn-large waves-effect waves-light green modal-trigger right" ><i class="material-icons">add</i></a>
    </div>
    <div class="row">
        <div class="col m8 offset-m2">

            <div class="m-t-3">
                <ul class="collection with-header">
                    <li class="collection-header"><h5>Все проекты {{ $user->name}}</h5></li>
                    @foreach($projects as $project)
                    <li class="collection-item">
                        <div>{{$project->project_name}}<a href="{{ route('detach_user')}}" data-token="{{ csrf_token() }}" data-user-id="{{$user->id}}" data-project-id="{{$project->id}}" class="detach-project secondary-content"><i class="material-icons red-text">clear</i></a></div>
                    </li>   
                    @endforeach
                </ul>                
            </div>
        </div>
    </div>
    <div id="add-modal" class="modal">
    <div class="modal-content">
        <form id="attach_user" action="{{ route('attach_user')}}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="user_id" value="{{ $user->id}}">
            <div class="input-field">
                <select id="user_id" name="project_id" required >
                    <option value="" selected>Выберите проект</option>
                    @foreach($allprojects as $project)
                    <option value="{{ $project->id}}">{{$project->project_name}}</option>
                    @endforeach
                </select>
                <label for="role">Проекты</label>
            </div>
            
        </form>
    </div>
    <div class="modal-footer">
        <button type="submit" form="attach_user" class="btn waves-effect waves-light green">Добавить</button>
    </div>
</div>
</div>
@endsection