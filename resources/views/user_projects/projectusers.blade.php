@extends('layouts.app')

@include('layouts.right_nav')
@section('content')
<div class="container">
    <div class="m-t-1">
        <a class="btn-floating btn-large waves-effect waves-light blue right_nav" data-activates="slide-out"><i class="material-icons">menu</i></a>
        <a href="#add-modal" class="btn-floating btn-large waves-effect waves-light green modal-trigger right" ><i class="material-icons">add</i></a>
    </div>
    <div class="row">
        <div class="col m8 offset-m2">

            <div class="m-t-3">
                <ul class="collection with-header">
                    <li class="collection-header"><h5>Пользователи проекта {{ $project->project_name}}</h5></li>
                    @foreach($users as $user)
                    <li class="collection-item">
                        <div>{{$user->name}}
                            <a href="{{ route('detach_user')}}" data-token="{{ csrf_token() }}" data-user-id="{{$user->id}}" data-project-id="{{$project->id}}" class="detach-project secondary-content"><i class="material-icons red-text">clear</i></a></div>
                    </li>   
                    @endforeach
                </ul>                
            </div>
        </div>
    </div>
</div>
<div id="add-modal" class="modal">
    <div class="modal-content">
        <form id="attach_user" action="{{ route('attach_user')}}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="project_id" value="{{ $project->id}}">
            <div class="input-field">
                <select id="user_id" name="user_id" required >
                    <option value="" selected>Выберите пользователя</option>
                    @foreach($allusers as $user)
                    <option value="{{ $user->id}}">{{$user->name}}</option>
                    @endforeach
                </select>
                <label for="role">Пользователи</label>
            </div>
            
        </form>
    </div>
    <div class="modal-footer">
        <button type="submit" form="attach_user" class="btn waves-effect waves-light green">Добавить</button>
    </div>
</div>
@endsection