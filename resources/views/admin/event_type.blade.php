@extends('layouts.app')

@include('layouts.right_nav')
@section('content')
<div class="container">
    <div class="m-t-1">
        <a class="btn-floating btn-large waves-effect waves-light blue right_nav" data-activates="slide-out"><i class="material-icons">menu</i></a>
        <a href="#add-modal" class="btn-floating btn-large waves-effect waves-light green modal-trigger right" ><i class="material-icons">add</i></a>
    </div>
    <div class="row">
        <div class="col m8 offset-m2">
            <h4 class="center-align">Типы событий</h4>
            <div class="m-t-3">
                <ul class="collapsible" data-collapsible="accordion">
                    @foreach($event_types as $event_type)
                    <li>
                        <div class="collapsible-header"><i class="material-icons">event_note</i>{{$event_type->event_type_name}}</div>
                        <div class="collapsible-body">
                            <form class="edit-event-type" action="{{ route('edit_event_type')}}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{$event_type->id}}">
                                <div class="input-field">
                                    <input type="text" id="name{{$event_type->id}}" name="name" required value="{{$event_type->event_type_name}}">
                                    <label for="name{{$event_type->id}}" >Имя</label>
                                </div>
                                <div class="input-field">
                                    <textarea id="description{{$event_type->id}}" name="description" class="materialize-textarea">{{$event_type->events_type_description}}</textarea>
                                    <label for="description{{$event_type->id}}" >Описание</label>
                                </div>
                                <div class="input-field">
                                    <select id="template{{$event_type->id}}" name="template" required>  
                                        <option value="">Выберите шаблон</option>        
                                        @foreach($templates as $template)
                                        <option value="{{$template}}" @if($event_type->events_type_template==$template) selected="selected" @endif >{{$template}}</option>
                                        @endforeach
                                    </select>
                                    <label for="template{{$event_type->id}}">Шаблон</label>
                                </div> 
                                <button type="submit" class="btn waves-effect black-text waves-light yellow">Изменить</button>
                                <a href="{{ route('delete_event_type')}}" id="del_{{$event_type->id}}" class="right-align event_type_delete tooltipped right" data-position="bottom" data-delay="50" data-id="{{$event_type->id}}" data-tooltip="Удалить"><i class="delete_Event_type red-text material-icons">clear</i></a>
                            </form>
                        </div>
                    </li>   
                    @endforeach
                </ul>                
            </div>
        </div>
    </div>
</div>
<div id="add-modal" class="modal">
    <div class="modal-content">
        <form id="add_event_type" action="{{ route('add_event_type')}}" method="POST">
            {{ csrf_field() }}
            <div class="input-field">
                <input type="text" id="name" name="name"  required >
                <label for="name" >Имя</label>
            </div>
            <div class="input-field">
                <textarea id="description" name="description" class="materialize-textarea"></textarea>
                <label for="description" >Описание</label>
            </div>
            <div class="input-field">
                <select id="template" name="template" required>  
                    <option value="">Выберите шаблон</option>        
                    @foreach($templates as $template)
                    <option value="{{$template}}">{{$template}}</option>
                    @endforeach
                </select>
                <label for="template">Шаблон</label>
            </div>
            
        </form>
    </div>
    <div class="modal-footer">
        <button type="submit" form="add_event_type" class="btn waves-effect waves-light green">Добавить</button>
    </div>
</div>
@endsection