@extends('layouts.app')

@include('layouts.right_nav')
@section('content')
<div class="container">
    <div class="m-t-1">
        <a class="btn-floating btn-large waves-effect waves-light blue right_nav" data-activates="slide-out"><i class="material-icons">menu</i></a>
        <a href="#add-modal" class="btn-floating btn-large waves-effect waves-light green modal-trigger right" ><i class="material-icons">add</i></a>
    </div>
    <div class="row">
        <div class="col m8 offset-m2">
            <h4 class="center-align">Пользователи</h4>
            <div class="m-t-3">
                <ul class="collapsible" data-collapsible="accordion">
                    @foreach($users as $user)
                    <li>
                        <div class="collapsible-header"><i class="material-icons">account_circle</i>{{$user->name}}</div>
                        <div class="collapsible-body">
                            <form class="edit-user" action="{{ route('edit_user')}}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{$user->id}}">
                                <div class="input-field">
                                    <input type="text" id="name{{$user->id}}" name="name" required value="{{$user->name}}">
                                    <label for="name{{$user->id}}" >Имя</label>
                                </div>
                                <div class="input-field">
                                    <input type="email" id="email{{$user->id}}" name="email" value="{{$user->email}}" required>
                                    <label for="email{{$user->id}}" >Email</label>
                                </div>
                                <div class="input-field">
                                    <select id="role{{$user->id}}" name="role" required>  
                                        
                                        <option value="admin" @if($user_top_role[$user->id]=='admin') selected @endif>admin</option>
                                        <option value="manager" @if($user_top_role[$user->id]=='manager') selected @endif>manager</option>
                                        <option value="user" @if($user_top_role[$user->id]=='user') selected @endif>user</option>
                                    </select>
                                    <label for="role{{$user->id}}">Роль</label>
                                </div>
                                <div class="input-field">
                                    <label for="password{{$user->id}}" class="col-md-4 control-label">Password</label>
                                    <input id="password{{$user->id}}" type="password" class="form-control" name="password">          
                                </div>
                                <div class="input-field">
                                    <label for="password-confirm{{$user->id}}" class="col-md-4 control-label">Confirm Password</label>
                                    <input id="password-confirm{{$user->id}}" type="password" class="form-control" name="password_confirmation" >
                                </div>
                                <button type="submit" class="btn waves-effect black-text waves-light yellow">Изменить</button>
                                <a href="{{ route('delete_user')}}" id="del_{{$user->id}}" class="right-align user_delete tooltipped right" data-position="bottom" data-delay="50" data-id="{{$user->id}}" data-tooltip="Удалить"><i class="delete_user red-text material-icons">clear</i></a>
                            </form>
                            <a href="{{ route('user_projects',['id'=>$user->id])}}">Проекты пользователя</a>
                        </div>
                    </li>   
                    @endforeach
                </ul>                
            </div>
        </div>
    </div>
</div>
<div id="add-modal" class="modal">
    <div class="modal-content">
        <form id="add_user" action="{{ route('add_user')}}" method="POST">
            {{ csrf_field() }}
            <div class="input-field">
                <input type="text" id="name" name="name"  required >
                <label for="name" >Имя</label>
            </div>
            <div class="input-field">
                <input type="email" id="email" name="email"  required>
                <label for="email" >Email</label>
            </div>
            <div class="input-field">
                <select id="role" name="role" required >
                    <option value="" selected>Выберите роль</option>
                    <option value="admin">admin</option>
                    <option value="manager">manager</option>
                    <option value="user">user</option>
                </select>
                <label for="role">Роль</label>
            </div>
            <div class="input-field">
                <label for="password" >Password</label>
                <input id="password" type="password"  name="password" required>          
            </div>
            <div class="input-field">
                <label for="password-confirm">Confirm Password</label>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <button type="submit" form="add_user" class="btn waves-effect waves-light green">Добавить</button>
    </div>
</div>
@endsection