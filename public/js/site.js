/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    $(".dropdown-button").dropdown();
    if ($('.right_nav').length) {
        $('.right_nav').sideNav();
    }
    if ($('.modal').length) {
        $('.modal').modal();
    }
    if ($('select').length) {
        $('select').material_select();
    }
    $('.edit-user').submit(function (e) {
        var form = $(this);
        e.preventDefault();
        $.ajax({
            method: 'POST',
            data: form.serialize(),
            url: form.attr('action'),
            datatype: 'json',
            success: function (data) {
                console.log(data);
            }
        });
    });
    $('#add_user').submit(function (e) {
        var form = $(this);
        e.preventDefault();
        $('#add-modal').modal('close');
        $.ajax({
            method: 'POST',
            data: form.serialize(),
            url: form.attr('action'),
            datatype: 'json',
            success: function (data) {
                if (data == 'success') {
                    Materialize.toast('Пользователь успешно добавлен.', 1000, 'green');
                    setTimeout(window.location.reload(), 1000);
                } else {
                    alert(data);
                    $('#add-modal').modal('open');
                }
            },
            error: function (data) {
                Materialize.toast('Ошибка.', 5000, 'red');
                $('#add-modal').modal('open');
                for (var key in data.responseJSON.errors) {
                    form.find("label[for='" + key + "']").addClass('active');
                    form.find("input[name='" + key + "']").addClass('invalid');
                    Materialize.toast(data.responseJSON.errors[key].join(), 5000, 'red');
                }
            }
        });
    });
    $('.edit-user').submit(function (e) {
        var form = $(this);
        e.preventDefault();
        $.ajax({
            method: 'PUT',
            data: form.serialize(),
            url: form.attr('action'),
            datatype: 'json',
            success: function (data) {
                if (data == 'success') {
                    Materialize.toast('Пользователь успешно обновлен.', 2000, 'green');
                    setTimeout(window.location.reload(), 3000);
                } else {
                    alert(data);
                }
            },
            error: function (data) {
                Materialize.toast('Ошибка.', 5000, 'red');
                for (var key in data.responseJSON.errors) {
                    form.find("input[name='" + key + "']").addClass('invalid');
                    Materialize.toast(data.responseJSON.errors[key].join(), 5000, 'red');
                }
            }
        });
    });
    $('.user_delete').click(function (e) {
        e.preventDefault();
        var link = $(this);
        var conf = confirm("Вы точно хотите удалить пользователя " + link.parent('form').find("input[name='name']").val() + "?");
        if (!conf)
            return;
        $.ajax({
            method: 'delete',
            url: link.attr('href'),
            datatype: 'json',
            data: 'id=' + link.attr('data-id') + '&_token=' + link.parent('form').find("input[name='_token']").val(),
            success: function (data) {
                if (data == 'success') {
                    Materialize.toast('Пользователь успешно удален.', 2000, 'green');
                    setTimeout(window.location.reload(), 3000);
                } else {
                    alert(data);
                }
            },
            error: function (data) {
                Materialize.toast('Ошибка.', 5000, 'red');
                for (var key in data.responseJSON.errors) {
                    Materialize.toast(data.responseJSON.errors[key].join(), 5000, 'red');
                }
            }
        });
    });
    $('#attach_user').submit(function (e) {
        var form = $(this);
        e.preventDefault();
        $('#add-modal').modal('close');
        $.ajax({
            method: 'POST',
            data: form.serialize(),
            url: form.attr('action'),
            datatype: 'json',
            success: function (data) {
                if (data == 'success') {
                    Materialize.toast('Успешно добавлен.', 1000, 'green');
                    setTimeout(window.location.reload(), 1000);
                } else {
                    alert(data);
                    $('#add-modal').modal('open');
                }
            },
            error: function (data) {
                Materialize.toast('Ошибка.', 5000, 'red');
                $('#add-modal').modal('open');
                for (var key in data.responseJSON.errors) {
                    form.find("label[for='" + key + "']").addClass('active');
                    form.find("input[name='" + key + "']").addClass('invalid');
                    Materialize.toast(data.responseJSON.errors[key].join(), 5000, 'red');
                }
            }
        });
    });
    $('.detach-project').click(function (e) {
        e.preventDefault();
        var link = $(this);
        var conf = confirm("Подтвердите действие");
        if (!conf)
            return;
        $.ajax({
            method: 'delete',
            url: link.attr('href'),
            datatype: 'json',
            data: 'user_id=' + link.attr('data-user-id') + '&project_id=' + link.attr('data-project-id') + '&_token=' + link.attr('data-token'),
            success: function (data) {
                if (data == 'success') {
                    Materialize.toast('Успешно.', 2000, 'green');
                    setTimeout(window.location.reload(), 3000);
                } else {
                    alert(data);
                }
            },
            error: function (data) {
                Materialize.toast('Ошибка.', 5000, 'red');
                for (var key in data.responseJSON.errors) {
                    Materialize.toast(data.responseJSON.errors[key].join(), 5000, 'red');
                }
            }
        })
    });
    $('#add_event_type').submit(function (e) {
        var form = $(this);
        e.preventDefault();
        $('#add-modal').modal('close');
        $.ajax({
            method: 'POST',
            data: form.serialize(),
            url: form.attr('action'),
            datatype: 'json',
            success: function (data) {
                if (data == 'success') {
                    Materialize.toast('Тип события добавлен.', 1000, 'green');
                    setTimeout(window.location.reload(), 1000);
                } else {
                    alert(data);
                    $('#add-modal').modal('open');
                }
            },
            error: function (data) {
                Materialize.toast('Ошибка.', 5000, 'red');
                $('#add-modal').modal('open');
                for (var key in data.responseJSON.errors) {
                    form.find("label[for='" + key + "']").addClass('active');
                    form.find("input[name='" + key + "']").addClass('invalid');
                    Materialize.toast(data.responseJSON.errors[key].join(), 5000, 'red');
                }
            }
        });
    });
    $('.event_type_delete').click(function (e) {
        e.preventDefault();
        var link = $(this);
        var conf = confirm("Вы точно хотите удалить тип события " + link.parent('form').find("input[name='name']").val() + "?");
        if (!conf)
            return;
        $.ajax({
            method: 'delete',
            url: link.attr('href'),
            datatype: 'json',
            data: 'id=' + link.attr('data-id') + '&_token=' + link.parent('form').find("input[name='_token']").val(),
            success: function (data) {
                if (data == 'success') {
                    Materialize.toast('Тип события успешно удален.', 2000, 'green');
                    setTimeout(window.location.reload(), 3000);
                } else {
                    alert(data);
                }
            },
            error: function (data) {
                Materialize.toast('Ошибка.', 5000, 'red');
                for (var key in data.responseJSON.errors) {
                    Materialize.toast(data.responseJSON.errors[key].join(), 5000, 'red');
                }
            }
        });
    });
    $('.edit-event-type').submit(function (e) {
        var form = $(this);
        e.preventDefault();
        $.ajax({
            method: 'PUT',
            data: form.serialize(),
            url: form.attr('action'),
            datatype: 'json',
            success: function (data) {
                if (data == 'success') {
                    Materialize.toast('Тип события успешно обновлен.', 2000, 'green');
                    setTimeout(window.location.reload(), 3000);
                } else {
                    alert(data);
                }
            },
            error: function (data) {
                Materialize.toast('Ошибка.', 5000, 'red');
                for (var key in data.responseJSON.errors) {
                    form.find("input[name='" + key + "']").addClass('invalid');
                    Materialize.toast(data.responseJSON.errors[key].join(), 5000, 'red');
                }
            }
        });
    });
    $('#add_event').submit(function (e) {
        var form = $(this);
        e.preventDefault();
        $('#add-modal').modal('close');
        $.ajax({
            method: 'POST',
            data: form.serialize(),
            url: form.attr('action'),
            datatype: 'json',
            success: function (data) {
                if (data == 'success') {
                    Materialize.toast('Cобытие добавлено.', 1000, 'green');
                    setTimeout(window.location.reload(), 1000);
                } else {
                    alert(data);
                    $('#add-modal').modal('open');
                }
            },
            error: function (data) {
                Materialize.toast('Ошибка.', 5000, 'red');
                $('#add-modal').modal('open');
                for (var key in data.responseJSON.errors) {
                    form.find("label[for='" + key + "']").addClass('active');
                    form.find("input[name='" + key + "']").addClass('invalid');
                    Materialize.toast(data.responseJSON.errors[key].join(), 5000, 'red');
                }
            }
        });
    });
    $('.edit-event').submit(function (e) {
        var form = $(this);
        e.preventDefault();
        $.ajax({
            method: 'PUT',
            data: form.serialize(),
            url: form.attr('action'),
            datatype: 'json',
            success: function (data) {
                if (data == 'success') {
                    Materialize.toast('События успешно обновлено.', 2000, 'green');
                    setTimeout(window.location.reload(), 3000);
                } else {
                    alert(data);
                }
            },
            error: function (data) {
                Materialize.toast('Ошибка.', 5000, 'red');
                for (var key in data.responseJSON.errors) {
                    form.find("input[name='" + key + "']").addClass('invalid');
                    Materialize.toast(data.responseJSON.errors[key].join(), 5000, 'red');
                }
            }
        });
    });
    $('.event_delete').click(function (e) {
        e.preventDefault();
        var link = $(this);
        var conf = confirm("Вы точно хотите удалить событие " + link.parent('form').find("input[name='name']").val() + "?");
        if (!conf)
            return;
        $.ajax({
            method: 'delete',
            url: link.attr('href'),
            datatype: 'json',
            data: 'id=' + link.attr('data-id') + '&_token=' + link.parent('form').find("input[name='_token']").val(),
            success: function (data) {
                if (data == 'success') {
                    Materialize.toast('Событие успешно удалено.', 2000, 'green');
                    setTimeout(window.location.reload(), 3000);
                } else {
                    alert(data);
                }
            },
            error: function (data) {
                Materialize.toast('Ошибка.', 5000, 'red');
                for (var key in data.responseJSON.errors) {
                    Materialize.toast(data.responseJSON.errors[key].join(), 5000, 'red');
                }
            }
        });
    });
    $('#add_page').submit(function (e) {
        var form = $(this);
        e.preventDefault();
        $('#add-modal').modal('close');
        $.ajax({
            method: 'POST',
            data: form.serialize(),
            url: form.attr('action'),
            datatype: 'json',
            success: function (data) {
                if (data == 'success') {
                    Materialize.toast('Страница добавлена.', 1000, 'green');
                    setTimeout(window.location.reload(), 1000);
                } else {
                    alert(data);
                    $('#add-modal').modal('open');
                }
            },
            error: function (data) {
                Materialize.toast('Ошибка.', 5000, 'red');
                $('#add-modal').modal('open');
                for (var key in data.responseJSON.errors) {
                    form.find("label[for='" + key + "']").addClass('active');
                    form.find("input[name='" + key + "']").addClass('invalid');
                    Materialize.toast(data.responseJSON.errors[key].join(), 5000, 'red');
                }
            }
        });
    });
});

