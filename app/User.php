<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function roles(){
        return $this->belongsToMany("App\\Role");
    }
    
    public function projects(){
        return $this->belongsToMany("App\\Project");
    }

    public function isEmployer(){
        return ($this->roles()->count()) ? TRUE : FALSE;
    }
    
    public function hasRole($role){
        return (in_array($role,$this->roles->pluck("name")->toArray()));
    }
    private function getIdInArray($array,$term){
        foreach ($array as $key=>$val){
            if($val==$term){
                return $key;
            }
        }
        
        throw new \Exception("Not valid role");
    }
    public function makeEmployer($title){
        $roles= Role::all()->pluck("name","id");
        $assigned_roles=array();
        switch ($title){
            case 'admin':
                $assigned_roles[]= $this->getIdInArray($roles, 'admin');
                $assigned_roles[]= $this->getIdInArray($roles, 'manager');
                $assigned_roles[]= $this->getIdInArray($roles, 'user');
                break;
            case 'manager':
                $assigned_roles[]= $this->getIdInArray($roles, 'manager');
                $assigned_roles[]= $this->getIdInArray($roles, 'user');
                break;
            case 'user':
                $assigned_roles[]= $this->getIdInArray($roles, 'user');
                break;
            default :
                throw new \Exception("Not valid user role");
        }
        if(!empty($assigned_roles)){
            $this->roles()->sync($assigned_roles);
        }else{
            throw new \Exception("Not valid user role");
        }
    }
    
    
    //ищем самую максимальную роль пользователя
    public function topRole($roles) {
        $top_role = '';
        foreach ($roles as $role) {
            if ($role->name == 'user' && !$top_role) {
                $top_role = 'user';
            }
            if ($role->name == 'manager') {
                $top_role = 'manager';
            }
            if ($role->name == 'admin') {
                $top_role = 'admin';
                break;
            }
        }
        return $top_role;
    }
}
