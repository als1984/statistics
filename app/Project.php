<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
//    public function users(){
//        return $this->belongsToMany("App\\User");
//    }
    public $timestamps=false;
    
    public function statistics(){
        return $this->hasMany("Statistic");
    }
    
    public function events(){
        return $this->hasMany('App\Event','projects_id','id');
    }
    
    public function pages(){
        return $this->hasMany('App\Page','project_id','id');
    }
}
