<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    public $timestamps=false;
    
//    public function statistics(){
//        return $this->hasMany('App\Statistics');
//    }
    
    public function projects(){
        return $this->belongsTo('App\Project', 'project_id', 'id');
    }
}
