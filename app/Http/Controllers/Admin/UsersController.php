<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UsersController extends Controller {

    public function index() {
        $users = User::all();
        $roles = Auth::user()->roles;
        $top_role = Auth::user()->topRole($roles);
        $user_top_role=array();
        foreach($users as $user){
            $user_top_role[$user->id]=$user->topRole($user->roles);
        }
        return view('admin.users', array('users' => $users, 'roles' => $roles, 'top_role' => $top_role,'user_top_role'=>$user_top_role));
    }

    public function add(Request $request) {
        $data = $request->all();
        $validator = Validator::make($data, [
                    'name' => 'required|string|max:255',
                    'email' => 'required|string|email|max:255|unique:users',
                    'password' => 'required|string|min:6|confirmed',
                    'role' => 'required|in:admin,manager,user',
                ])->validate();

        $user = new User;
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = bcrypt($data['password']);
        $user->save();
        $user->makeEmployer($data['role']);
        $user->save();
        return 'success';
    }

    public function edit(Request $request) {
        $data = $request->all();
        $validator = Validator::make($data, [
                    'id' => 'required|integer',
                    'name' => 'required|string|max:255',
                    'email' => ['email', Rule::unique('users')->ignore($data['id'])],
                    'role' => 'required|in:admin,manager,user',
                ])->validate();
        if (isset($data['password'])) {
            $validator = Validator::make($data, [
                        'password' => 'required|string|min:6|confirmed',
                    ])->validate();
        }
        $user= User::findOrFail($data['id']);
        $user->name=$data['name'];
        $user->email = $data['email'];
        if($data['password']){
            $user->password = bcrypt($data['password']);
        }
        $user->save();
        $user->makeEmployer($data['role']);
        $user->save();
        return 'success';
    }
    
    public function delete(Request $request){
        $data = $request->all();
        $validator = Validator::make($data, [
                    'id' => 'required|integer',
                ])->validate();
        if((int)$data['id']==Auth::user()->id){
            return 'Вы не можете удалить себя!!!';
        }
        $user=User::findOrFail($data['id']);
        $user->delete();
        return 'success';
    }

    
    

}
