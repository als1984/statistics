<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Project;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class UserProjectController extends Controller {

    //все пользователи проекта
    public function usersToProjects(int $id) {
        if (!$id) {
            abort(404);
        }
        $project = Project::find($id);
        if (!$project) {
            abort(404);
        }
        $users = User::whereHas('projects', function ($q) use ($id) {
                    $q->where('id', '=', $id);
                })->get();
        $roles = Auth::user()->roles;
        $allusers=User::all();
        return view('user_projects.projectusers', 
                ['users' => $users, 
                    'roles' => $roles, 
                    'project' => $project,
                    'allusers'=>$allusers]);
    }

    //все проекты пользователя
    public function projectsToUsers(int $id) {
        if (!$id) {
            abort(404);
        }
        $user = User::find($id);
        if (!$user) {
            abort(404);
        }
        $projects = $user->projects;
        $allprojects= Project::all();
        $roles = Auth::user()->roles;
        return view('user_projects.userprojects', 
                ['user' => $user,
                    'roles' => $roles, 
                    'projects' => $projects,
                    'allprojects'=>$allprojects]);
    }

    public function attach(Request $request) {
        $data = $request->all();
        $validator = Validator::make($data, [
                    'user_id' => 'required|integer',
                    'project_id' => 'required|integer'])->validate();
        $user= User::findOrFail((int) $data['user_id']);
        $project = Project::findOrFail((int) $data['project_id']);
        if($user->projects->contains((int) $data['project_id'])){
            return 'Пользователь уже есть в проекте';
        }
        $user->projects()->attach($project);
        return 'success';
    }
    
    public function detach(Request $request) {
        $data = $request->all();
        $validator = Validator::make($data, [
                    'user_id' => 'required|integer',
                    'project_id' => 'required|integer'])->validate();
        $user= User::findOrFail((int) $data['user_id']);
        $project = Project::findOrFail((int) $data['project_id']);
        if(!$user->projects->contains((int) $data['project_id'])){
            return 'Пользователя нет в проекте';
        }
        $user->projects()->detach($project);
        return 'success';
    }

}
