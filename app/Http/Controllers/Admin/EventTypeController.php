<?php

namespace App\Http\Controllers\Admin;

use App\Events_type;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class EventTypeController extends Controller
{
    public function index(){
        $roles= Auth::user()->roles;
        $event_types= Events_type::all();
        $tpl=new Events_type;
        $templates= $tpl->getTemplates();
        $tpl=null;
        //$templates=array();
        return view('admin.event_type',array('event_types'=>$event_types,'roles'=>$roles,'templates'=>$templates));
    }
    
    public function add(Request $request){
        $data = $request->all();
        $event_type=new Events_type;
        $validator = Validator::make($data, [
                    'name' => 'required|string|max:255',
                    'description' => 'required|string',
                    'template' => ['required',
                        Rule::in($event_type->getTemplates())],
                ])->validate();
        $event_type->event_type_name=$data['name'];
        $event_type->events_type_template=$data['template'];
        $event_type->events_type_description=$data['description'];
        $event_type->save();
        return 'success';
    }
    
    public function delete(Request $request){
        $data = $request->all();
        $validator = Validator::make($data, [
                    'id' => 'required|integer',
                ])->validate();
        $event_type= Events_type::findOrFail($data['id']);
        $event_type->delete();
        return 'success';
    }
    
    public function edit(Request $request){
        $data = $request->all();
        $event_type=new Events_type;
        $validator = Validator::make($data, [
                    'id' => 'required|integer',
                    'name' => 'required|string|max:255',
                    'description' => 'required|string',
                    'template' => ['required',
                        Rule::in($event_type->getTemplates())],
                ])->validate();
        $event_type= Events_type::findOrFail($data['id']);
        $event_type->event_type_name=$data['name'];
        $event_type->events_type_template=$data['template'];
        $event_type->events_type_description=$data['description'];
        $event_type->save();
        return 'success';
    }
}
