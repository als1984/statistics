<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Page;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class PageController extends Controller {

    public function getProjectPages(int $id) {
        $user = Auth::user();
        $projects = $user->projects;
        $project = $user->projects()->findOrFail($id);
        $roles = $user->roles;
        $pages = Page::with(['projects' => function($query) use ($id) {
                        $query->where('id', '=', $id);
                    }])->paginate(50);
        return view('user.page', array('pages' => $pages,
            'roles' => $roles,
            'projects' => $projects,
            'project' => $project,
            'pagelinks'=>true));
    }

    public function addManual(Request $request) {
        $data = $request->all();
        $validator = Validator::make($data, [
                    'name' => 'required|string|max:255',
                    'url' => 'required|url|max:255',
                    'project_id' => 'required|integer',
                ])->validate();
        $user=Auth::user();
        $project=$user->projects()->findOrFail($data['project_id']);
        $page=new Page;
        $page->page_name=$data['name'];
        $page->page_url=$data['url'];
        $page->projects()->associate($project);
        $page->save();
        return 'success';
    }
    
    public function allMyPage(){
        $user = Auth::user();
        $projects = $user->projects;
        $pages=$projects->load('pages');
        $roles = $user->roles;
        return view('user.page', array('pages' => $pages,
            'roles' => $roles,
            'projects' => $projects,
            'project' => null,
            'pagelinks'=>false));
    }

}
