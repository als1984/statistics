<?php

namespace App\Http\Controllers\Manager;

use App\Project;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ProjectController extends Controller
{
    
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $projects = $user->projects()->paginate(20);
        $roles = $user->roles;
        return view('manager.projects',array('projects'=>$projects,'roles'=>$roles));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles= Auth::user()->roles;
        return view('manager.addproject', array('roles'=>$roles));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data,['project_name' => 'required|string|max:255'])->validate();
        $project=new Project;
        $project->project_name=$data['project_name'];
        $project->project_key= md5($data['project_name'].'key_generate');
        $project->save();
        $user= Auth::user();
        $user->projects()->attach($project->id);
        $projects = $user->projects()->paginate(20);
        $roles = $user->roles;
        return view('manager.projects',array('projects'=>$projects,'roles'=>$roles));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,Project $project)
    {
        $this->checkProjectOwner($project);
        $roles= Auth::user()->roles;
        return view('manager.showproject', array('roles'=>$roles, 'project'=>$project));                
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        $this->checkProjectOwner($project);
        $roles= Auth::user()->roles;
        return view('manager.editproject', array('roles'=>$roles,'project'=>$project));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        $data=$request->all();
        $validator = Validator::make($data, [
                    'project_name'=>'required|string|max:255',
                ])->validate();
        $this->checkProjectOwner($project);
        $project->project_name=$data['project_name'];
        $project->project_key=md5($data['project_name'].'key_generate');
        $project->save();
        return redirect('projects');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        $this->checkProjectOwner($project);
        $project->delete();
        return redirect('projects');
    }
    
    public function all(){
        $projects= Project::paginate(20);
        $roles = Auth::user()->roles;
        return view('manager.projects',array('projects'=>$projects,'roles'=>$roles));   
    }
    
    public function checkProjectOwner(Project $project){
        $user= Auth::user();
        
        if(!$user->hasRole('admin') || !$user->projects->contains($project->id)){
            return redirect('home');
        }
    }
    
}
