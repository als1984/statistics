<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Event;
use App\Events_type;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class EventController extends Controller
{
    public function index(){
        $user= Auth::user();
        $roles= $user->roles;
        $projects=$user->projects;
        $events=[];
        if($projects){
            foreach($projects as $project){
                //var_dump($project->events);die();
                if($project->events->first()){
                    $events[]=$project->events;
                }
            }
        }
        //var_dump($events);die();
        $events_types= Events_type::all();
        return view('manager.event',array('events_types'=>$events_types,
            'roles'=>$roles,
            'projects'=>$projects,
            'events'=>$events));
    }
    
    public function add(Request $request){
        $data = $request->all();
        $validator = Validator::make($data, [
                    'name' => 'required|string|max:255',
                    'element' => 'required|string|max:255',
                    'url_element'=>'nullable|string',
                    'name_element'=>'nullable|string',
                    'project_id' => 'required|integer',
                    'event_type_id'=>'required|integer',
                ])->validate();
        $project=Auth::user()->projects()->findOrFail($data['project_id']);
        $event_type= Events_type::findOrFail($data['event_type_id']);
        $event= new Event;
        $event->event_name=$data['name'];
        $event->event_element=$data['element'];
        $event->event_url_element=$data['url_element'] ? $data['url_element'] : 'window.location.href';
        $event->event_name_element=$data['name_element'] ? $data['name_element'] : 'title';
        $event->projects()->associate($project);
        $event->events_types()->associate($event_type);
        $event->save();
        return 'success';
    }
    
    public function edit(Request $request){
        $data = $request->all();
        $validator = Validator::make($data, [
                    'id' => 'required|integer',
                    'name' => 'required|string|max:255',
                    'element' => 'required|string|max:255',
                    'url_element'=>'nullable|string',
                    'name_element'=>'nullable|string',
                    'project_id' => 'required|integer',
                    'event_type_id'=>'required|integer',
                ])->validate();
        $project=Auth::user()->projects()->findOrFail($data['project_id']);
        $event_type= Events_type::findOrFail($data['event_type_id']);
        $event= Event::findOrFail($data['id']);
        $event->event_name=$data['name'];
        $event->event_element=$data['element'];
        $event->event_url_element=$data['url_element'] ? $data['url_element'] : 'window.location.href';
        $event->event_name_element=$data['name_element'] ? $data['name_element'] : 'title';
        $event->projects()->associate($project);
        $event->events_types()->associate($event_type);
        $event->save();
        return 'success';
    }
    
    public function delete(Request $request){
        $data = $request->all();
        $validator = Validator::make($data, [
                    'id' => 'required|integer',
                ])->validate();
        $event= Event::findOrFail($data['id']);
        $project= Auth::user()->projects()->find($event->projects_id);
        if(!$project){
            return 'You hasn`t premissions to delete this event!';
        }
        $event->delete();
        return 'success';
    }
    
    public function getProjectEvents(int $id){
        $user= Auth::user();
        $roles= $user->roles;
        $projects=$user->projects;
        $project=$user->projects()->findOrFail($id);
        $events=[];
        if($project){
            $events[]=$project->events;                            
        }
        //var_dump($events);die();
        $events_types= Events_type::all();
        return view('manager.event',array('events_types'=>$events_types,
            'roles'=>$roles,
            'projects'=>$projects,
            'events'=>$events));
    }
}
