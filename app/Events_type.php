<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Events_type extends Model {

    public $timestamps = false;

    public function events() {
        return $this->hasMany('Event');
    }

    public function getTemplates() {
        $templates = array();
        $tpl_path = resource_path('views/events_types/');
        foreach (glob($tpl_path . '*.blade.php') as $filename) {
            $templates[] = basename($filename);
        }
        //$templates= scandir($tpl_path);
        return $templates;
    }

}
