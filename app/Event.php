<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    public $timestamps=false;
    
    public function projects(){
        return $this->belongsTo('App\Project');
    }
    
    public function events_types(){
        return $this->belongsTo('App\Events_type');
    }
}
