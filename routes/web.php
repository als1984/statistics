<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['checkrole:admin'])->group(function (){
    Route::get('/users','Admin\UsersController@index')->name('users');
    Route::post('/users','Admin\UsersController@add')->name('add_user');
    Route::put('/users','Admin\UsersController@edit')->name('edit_user');
    Route::delete('/users','Admin\UsersController@delete')->name('delete_user');
    Route::get('projects/all','Manager\ProjectController@all')->name('all_projects');
    Route::get('project-users/{id}','Admin\UserProjectController@usersToProjects')->name('project_users');
    Route::get('user-projects/{id}','Admin\UserProjectController@projectsToUsers')->name('user_projects');
    Route::post('project-users','Admin\UserProjectController@attach')->name('attach_user');
    Route::delete('project-users','Admin\UserProjectController@detach')->name('detach_user');
    Route::get('event-type','Admin\EventTypeController@index')->name('event_type');
    Route::post('event-type','Admin\EventTypeController@add')->name('add_event_type');
    Route::delete('event-type','Admin\EventTypeController@delete')->name('delete_event_type');
    Route::put('event-type','Admin\EventTypeController@edit')->name('edit_event_type');
});

Route::middleware(['checkrole:manager'])->group(function (){
    Route::resource('projects','Manager\ProjectController');
    Route::get('event','Manager\EventController@index')->name('events');
    Route::post('event','Manager\EventController@add')->name('add_event');
    Route::put('event','Manager\EventController@edit')->name('edit_event');
    Route::delete('event','Manager\EventController@delete')->name('delete_event');
    Route::get('project/{id}/events','Manager\EventController@getProjectEvents')->name('project_events');
});

Route::middleware(['checkrole:user'])->group(function (){
    Route::get('project/{id}/pages','User\PageController@getProjectPages')->name('get_project_pages');
    Route::post('pages','User\PageController@addManual')->name('add_page');
    Route::get('pages','User\PageController@allMyPage')->name('my_page');
});