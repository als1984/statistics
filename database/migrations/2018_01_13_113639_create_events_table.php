<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('event_name');
            $table->string('event_element');
            $table->string('event_url_element');
            $table->string('event_name_element');
            $table->integer('projects_id')->unsigned();
            $table->foreign('projects_id')->references('id')->on('projects')->onDelete('cascade');
            $table->integer('events_types_id')->unsigned();
            $table->foreign('events_types_id')->references('id')->on('events_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
