<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user= new User;
        $user->name = 'admin';
        $user->email = 'sys.aleksander@grizzly.by';
        $user->password = bcrypt('gr002302');
        $user->save();
        $user->makeEmployer('admin');
        $user->save();
    }
}
