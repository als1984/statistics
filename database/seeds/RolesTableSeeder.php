<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin= Role::create([
            "name"=>'admin'
        ]);
        $manager= Role::create([
            "name"=>'manager'
        ]);
        $user= Role::create([
            "name"=>'user'
        ]);
    }
}
